---
title: Astro官方文档—Integrations4学习笔记
description: Astro官方文档—Integrations4学习笔记
---

1、Astro DB 是专为 Astro 设计的完全托管的 SQL 数据库。本地开发或连接到我们 Astro Studio 平台上管理的托管数据库。

2、Astro DB的更多详细内容见文档：https://docs.astro.build/en/guides/astro-db/