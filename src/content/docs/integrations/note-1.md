---
title: Astro官方文档—Integrations1学习笔记
description: Astro官方文档—Integrations1学习笔记
---

1、官方维护的astro集成

![](https://cdn.sa.net/2024/05/18/I6Do5W78u4clpBR.webp)

2、集成依赖性问题：

如果您在添加集成后看到任何类似 Cannot find package '[package-name]' 的警告，则您的包管理器可能尚未为您安装对等依赖项。要安装这些缺少的软件包，请运行 npm install [package-name] 。

3、将集成导入到astro项目中的三种常用方法

![](https://cdn.sa.net/2024/05/18/GZ5khcLsiEXo82K.webp)

4、要一次性升级所有官方集成，请运行 @astrojs/upgrade 命令。这会将 Astro 和所有官方集成升级到最新版本。