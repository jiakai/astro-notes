---
title: Astro官方文档—Integrations3学习笔记
description: Astro官方文档—Integrations3学习笔记
---


1、Astro 允许您为部分或全部页面和端点选择按需渲染。这也称为服务器端渲染 (SSR)：根据请求在服务器上生成 HTML 页面并将其发送到客户端。适配器用于在服务器上运行您的项目并处理这些请求。

2、astro考虑启用SSR的使用场景

![](https://cdn.sa.net/2024/05/22/YgVpkCQ7EUGyltH.webp)

3、启用按需服务器端渲染

![](https://cdn.sa.net/2024/05/22/faLiXkznlhKg5Sp.webp)

4、要以 server 或 hybrid 模式部署项目，您需要添加适配器。这是因为这两种模式都需要服务器运行时：在服务器上运行代码以在请求时生成页面的环境。每个适配器都允许 Astro 输出在特定运行时（例如 Vercel、Netlify 或 Cloudflare）上运行项目的脚本。

5、添加适配器。

6、修改响应标头的功能仅在页面级别可用。 （您不能在组件内部使用它们，包括布局组件。）当 Astro 运行组件代码时，它已经发送了响应标头，并且无法修改它们。