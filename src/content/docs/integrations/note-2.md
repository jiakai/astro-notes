---
title: Astro官方文档—Integrations2学习笔记
description: Astro官方文档—Integrations2学习笔记
---

1、使用框架组件

![](https://cdn.sa.net/2024/05/18/4bazvEhLj2CTJrl.webp)、

2、可以使用 client:* 指令使框架组件具有交互性（水合）。这些组件属性决定组件的 JavaScript 何时应发送到浏览器。

3、对于除 client:only 之外的所有客户端指令，您的组件将首先在服务器上呈现以生成静态 HTML。组件 JavaScript 将根据您选择的指令发送到浏览器。然后该组件将水合并变得具有交互性。

4、渲染组件所需的 JavaScript 框架（React、Svelte 等）将与组件自己的 JavaScript 一起发送到浏览器。如果页面上的两个或多个组件使用相同的框架，则该框架只会发送一次。

5、当这些组件在 Astro 中使用时，大多数特定于框架的可访问性模式应该以相同的方式工作。请务必选择一个客户端指令，以确保在适当的时间正确加载和执行任何与可访问性相关的 JavaScript！

6、可用的hydration指令

![](https://cdn.sa.net/2024/05/18/q9fKwLv6t2Bsi8U.webp)

7、您可以在同一个 Astro 组件中导入和渲染来自多个框架的组件。

注意点：只有 Astro 组件 ( .astro ) 可以包含来自多个框架的组件。

8、将函数作为props传递的注意点

![](https://cdn.sa.net/2024/05/18/Qe1PFbrkIBLzRXy.webp)

9、将子组件传递给框架组件

在 Astro 组件内部，您可以将子组件传递给框架组件。每个框架对于如何引用这些子框架都有自己的模式：React、Preact 和 Solid 都使用名为 children 的特殊 prop，而 Svelte 和 Vue 使用 <slot /> 元素。

10、在 Astro 文件内部，框架组件子组件也可以是水合组件。这意味着您可以递归地嵌套来自任何这些框架的组件。

11、请记住：框架组件文件本身（例如 .jsx 、 .svelte ）不能混合多个框架。

12、Astro 组件始终呈现为静态 HTML，即使它们包含水合的框架组件也是如此。这意味着您只能传递不执行任何 HTML 渲染的 props。将 React 的“渲染属性”从 Astro 组件传递到框架组件将不起作用，因为 Astro 组件无法提供此模式所需的客户端运行时行为。相反，请使用命名槽。

13、任何 UI 框架组件都会成为该框架的一个“孤岛”。这些组件必须完全编写为该框架的有效代码，仅使用其自己的导入和包。您无法在 UI 框架组件中导入 .astro 组件（例如 .jsx 或 .svelte ）。

14、Astro 组件是纯 HTML 模板组件，没有客户端运行时。但是，您可以在 Astro 组件模板中使用 `<script>` 标记将 JavaScript 发送到在全局范围内执行的浏览器。
