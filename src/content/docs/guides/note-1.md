---
title: Astro官方文档—Guides1学习笔记
description: Astro官方文档—Guides1学习笔记
---

1、Astro 使用基于文件的路由根据项目 src/pages/ 目录的文件布局生成构建 URL。

2、Astro 使用标准 HTML <a> 元素在路线之间导航。没有提供特定于框架的 <Link> 组件。

3、src/pages/ 目录中的 .astro 页面组件以及 Markdown 和 MDX 文件（ .md 、 .mdx ）会自动成为您网站上的页面。每个页面的路由对应于 src/pages/ 目录中的路径和文件名。

4、Astro 项目中没有单独的“路由配置”需要维护！当您将文件添加到 src/pages/ 目录时，会自动为您创建一条新路由。在静态构建中，您可以使用 build.format 配置选项自定义文件输出格式。

5、动态路由

![](https://cdn.sa.net/2024/05/22/7YEljbozqTIc9WJ.webp)

## SSG模型

6、由于所有路由都必须在构建时确定，因此动态路由必须导出 getStaticPaths() ，该 getStaticPaths() 返回具有 params 属性的对象数组。这些对象中的每一个都会生成相应的路线。

7、文件名可以包含多个参数，这些参数必须全部包含在 getStaticPaths() 中的 params 对象中：

8、如果您的 URL 路由需要更大的灵活性，您可以在 .astro 文件名中使用剩余参数 ( [...path] ) 来匹配任意深度的文件路径：

## SSR模式

9、在 SSR 模式下，动态路由的定义方式相同：在文件名中包含 [param] 或 [...path] 括号以匹配任意字符串或路径。但由于路由不再提前构建，因此页面将被提供给任何匹配的路由。由于这些不是“静态”路由，因此不应使用 getStaticPaths 。

10、由于 SSR 页面无法使用 getStaticPaths() ，因此它们无法接收 props。前面的示例可以通过查找对象中 slug 参数的值来适应 SSR 模式。如果路由位于根（“/”），则 slug 参数将为 undefined 。如果对象中不存在该值，我们将重定向到 404 页面。

11、您可以使用 redirects 值在 Astro 配置中指定永久重定向的映射。

12、路由优先顺序

![](https://cdn.sa.net/2024/05/22/YV5IzM9wmdKSAQ8.webp)

![](https://cdn.sa.net/2024/05/22/WQSptKMyLiwON9R.webp)

13、Astro 支持针对需要拆分为多个页面的大量数据的内置分页。 Astro 将生成常见的分页属性，包括上一页/下一页 URL、总页数等。

14、您可以通过在页面或目录名称前添加下划线前缀 ( _ ) 来排除页面或目录的构建。带有 _ 前缀的文件不会被路由器识别，也不会被放入 dist/ 目录中。