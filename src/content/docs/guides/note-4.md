---
title: Astro官方文档—Guides4学习笔记
description: Astro官方文档—Guides4学习笔记
---

1、Astro 旨在让 CSS 样式设计和编写变得轻而易举。直接在 Astro 组件中编写您自己的 CSS 或导入您最喜欢的 CSS 库（如 Tailwind）。还支持 Sass 和 Less 等高级样式语言。

2、设置 Astro 组件的样式就像向组件或页面模板添加 `<style>` 标签一样简单。当您将 `<style>` 标签放入 Astro 组件内时，Astro 将自动检测 CSS 并为您处理样式。

3、Astro `<style>` CSS 规则默认自动确定范围。作用域样式在幕后编译，仅适用于同一组件内编写的 HTML。

4、虽然我们建议大多数组件使用作用域样式，但您最终可能会找到编写全局、无作用域 CSS 的正当理由。您可以使用 `<style is:global>` 属性选择退出自动 CSS 作用域。

5、css规则的优先级

![](https://cdn.sa.net/2024/05/22/oMersB8TSRkp4vu.webp)

6、绕过 Astro 的内置 CSS 捆绑时要小心！样式不会自动包含在构建的输出中，您有责任确保引用的文件正确包含在最终页面输出中。