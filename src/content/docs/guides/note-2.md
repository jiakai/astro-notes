---
title: Astro官方文档—Guides2学习笔记
description: Astro官方文档—Guides2学习笔记
---

1、Markdown 通常用于创作文本较多的内容，例如博客文章和文档。 Astro 内置了对标准 Markdown 文件的支持，这些文件还可以包含 frontmatter YAML 来定义自定义元数据，例如标题、描述和标签。

2、安装了 @astrojs/mdx 集成后，Astro 还支持 MDX ( .mdx ) 文件，这些文件带来了附加功能，例如支持 JavaScript 表达式和 Markdown 内容中的组件。

3、您可以在 Astro 中的特殊 src/content/ 文件夹中管理 Markdown 和 MDX 文件。内容集合可帮助您组织内容、验证 frontmatter，并在处理内容时提供自动 TypeScript 类型安全。

4、Astro 将 /src/pages/ 目录内的任何 .md （或替代支持的扩展）或 .mdx 文件视为页面。将文件放入此目录或任何子目录中，将使用文件的路径名自动构建页面路由。

5、其余有关md和mdx的内容见文档：https://docs.astro.build/en/guides/markdown-content/