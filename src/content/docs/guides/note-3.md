---
title: Astro官方文档—Guides3学习笔记
description: Astro官方文档—Guides3学习笔记
---

1、您可以使用标准 HTML <script> 标签向 Astro 组件添加交互性，而无需使用 React、Svelte、Vue 等 UI 框架。这允许您发送 JavaScript 以在浏览器中运行并向 Astro 组件添加功能。

2、脚本处理流程

![](https://cdn.sa.net/2024/05/22/4wB1LgHFcQiNEox.webp)

3、async 属性对于普通脚本很有价值，因为它可以防止它们阻塞渲染。但是，模块脚本已经具有此行为。将 async 添加到模块脚本将导致其在页面完全加载之前执行。这可能不是您想要的。

4、要阻止 Astro 处理脚本，请添加 is:inline 指令。

5、在某些情况下，Astro 不会处理您的脚本标签。特别是，将 type="module" 或 src 之外的任何属性添加到 `<script>` 标记将导致 Astro 将该标记视为具有 is:inline。

6、要加载项目 src/ 文件夹外部的脚本，请包含 is:inline 指令。当您如上所述导入脚本时，此方法会跳过 Astro 提供的 JavaScript 处理、捆绑和优化。

7、一些 UI 框架使用自定义语法进行事件处理，例如 onClick={...} (React/Preact) 或 @click="..." (Vue)。 Astro 更严格地遵循标准 HTML，并且不使用事件的自定义语法。