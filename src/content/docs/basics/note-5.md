---
title: Astro官方文档—Basics5学习笔记
description: Astro官方文档—Basics5学习笔记
---

1、Astro 组件语法是 HTML 的超集。该语法旨在让任何有编写 HTML 或 JSX 经验的人都感到熟悉，并添加了对包含组件和 JavaScript 表达式的支持。

2、您可以在 Astro 组件的两个代码围栏 ( --- ) 之间的 frontmatter 组件脚本内部定义本地 JavaScript 变量。然后，您可以使用类似 JSX 的表达式将这些变量注入到组件的 HTML 模板中！

3、可以使用花括号语法将局部变量添加到 HTML 中。

4、局部变量可以用在大括号中来将属性值传递给 HTML 元素和组件。

5、注意点

![](https://cdn.sa.net/2024/05/16/1oBVMf5XU2NkGhw.webp)

6、Astro 可以使用 JSX 逻辑运算符和三元表达式有条件地显示 HTML。

7、您还可以通过将变量设置为 HTML 标签名称或组件导入来使用动态标签。

8、使用动态标签的注意点

![](https://cdn.sa.net/2024/05/16/ghecAsWNnMvzfDr.webp)

9、Astro 支持使用 <Fragment> </Fragment> 或简写 <> </>；添加 set:* 指令时，片段可用于避免包装元素。

10、astro与jsx之间的差异：

A、

Astro和JSX在处理HTML属性时的不同。在JSX中，我们通常使用驼峰命名法（camelCase）来命名HTML属性。例如，一个常见的HTML属性tabindex在JSX中会被写作tabIndex。

然而，在Astro中，所有的HTML属性都使用短横线分隔命名法（kebab-case）。也就是说，我们会使用tab-index而不是tabIndex。这种命名法甚至适用于class属性，这在React中是不支持的（在React中，我们通常使用className而不是class）。

简单来说，Astro在命名HTML属性时更接近原生HTML的风格。

B、Astro 组件模板可以呈现多个元素，无需将所有内容包装在单个 <div> 或 <> 中，这与 JavaScript 或 JSX 不同。

C、在 Astro 中，您可以使用标准 HTML 注释或 JavaScript 样式注释。

注意点：HTML 风格的注释将包含在浏览器 DOM 中，而 JS 风格的注释将被跳过。要留下 TODO 消息或其他仅用于开发的说明，您可能希望使用 JavaScript 风格的注释。