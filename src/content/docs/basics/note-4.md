---
title: Astro官方文档—Basics4学习笔记
description: Astro官方文档—Basics4学习笔记
---

1、关于布局组件的介绍

![](https://cdn.sa.net/2024/05/16/N9EjzGLrsMeK12w.webp)

2、页面布局对于 Markdown 和 MDX 页面特别有用，否则它们不会有任何页面格式。Astro 提供了一个特殊的 layout frontmatter 属性来指定使用哪个 .astro 组件作为页面布局。

3、md或者mdx页面的典型布局

![](https://cdn.sa.net/2024/05/16/9sAPxaTqC2wEoGB.webp)

4、可以使用 MarkdownLayoutProps 或 MDXLayoutProps 帮助器设置布局的 Props 类型。

5、布局可以访问的props(properties，属性)：

![](https://cdn.sa.net/2024/05/16/F4M2agbZA18otJy.webp)

6、md/mdx布局访问从Astro.props导出的属性的注意点：

![](https://cdn.sa.net/2024/05/16/mgDvGUIokRNJ9jZ.webp)

7、可以对md、mdx、astro使用同一个布局。

8、布局组件不需要包含整个页面的 HTML。您可以将布局分解为更小的组件，并组合布局组件以创建更灵活的页面模板。当您想要跨多个布局共享某些代码时，此模式非常有用。