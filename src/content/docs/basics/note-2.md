---
title: Astro官方文档—Basics2学习笔记
description: Astro官方文档—Basics2学习笔记
---

1、Astro 组件是任何 Astro 项目的基本构建块。它们是纯 HTML 模板组件，没有客户端运行时。您可以通过文件扩展名来识别 Astro 组件： .astro 。

2、Astro 组件非常灵活。通常，Astro 组件会在页面上包含一些可重用的 UI，例如标题或个人资料卡。有时，Astro 组件可能包含较小的 HTML 片段，例如使 SEO 易于使用的常见 <meta> 标记的集合。 Astro 组件甚至可以包含整个页面布局。

3、关于 Astro 组件最重要的一点是它们不会在客户端上渲染。它们在构建时或按需使用服务器端渲染 (SSR) 渲染为 HTML。您可以在组件 frontmatter 中包含 JavaScript 代码，所有这些代码都将从发送到用户浏览器的最终页面中删除。结果是网站速度更快，默认情况下添加的 JavaScript 占用量为零。

4、当您的 Astro 组件确实需要客户端交互性时，您可以添加标准 HTML <script> 标记或 UI Framework 组件。

5、组件结构

Astro 组件由两个主要部分组成：组件脚本和组件模板。每个部分执行不同的工作，但它们共同提供了一个既易于使用又具有足够表现力的框架，足以处理您可能想要构建的任何内容。

```astro
---
// Component Script (JavaScript)
---
<!-- Component Template (HTML + JS Expressions) -->
```

6、组件脚本

Astro 使用代码围栏 ( --- ) 来识别 Astro 组件中的组件脚本。如果您以前写过 Markdown，您可能已经熟悉一个类似的概念，称为 frontmatter。 Astro 组件脚本的想法直接受到这个概念的启发。

您甚至可以在组件脚本中编写 TypeScript！

7、组件模板位于代码围栏下方，决定组件的 HTML 输出。

Astro 的组件模板语法还支持 JavaScript 表达式、Astro <style> 和 <script> 标记、导入的组件以及特殊的 Astro 指令。组件脚本中定义的数据和值可以在组件模板中使用来生成动态创建的 HTML。

8、组件被设计为可重用和可组合。您可以使用其他组件内部的组件来构建越来越高级的 UI

9、组件的props

Astro 组件可以定义和接受 props。然后，这些 props 可用于组件模板来渲染 HTML。道具可在 frontmatter 脚本的 Astro.props 全局中使用。

10、您还可以使用带有 Props 类型接口的 TypeScript 定义您的 props。 Astro 会自动选取 frontmatter 中的 Props 界面并给出类型警告/错误。当从 Astro.props 解构时，这些属性也可以被赋予默认值。

11、插槽

<slot /> 元素是外部 HTML 内容的占位符，允许您将其他文件中的子元素注入（或“插入”）到组件模板中。

默认情况下，传递给组件的所有子元素都将在其 <slot /> 中呈现。

与 props 不同的是，props 是传递给 Astro 组件的属性，可通过 Astro.props 在整个组件中使用，而 slot 则在编写子 HTML 元素的位置呈现它们。

12、Astro 组件也可以有命名槽。这允许您仅将具有相应插槽名称的 HTML 元素传递到插槽的位置。

插槽使用 name 属性命名。

13、请注意，命名槽必须是组件的直接子级。您不能通过嵌套元素传递命名槽。

14、命名槽也可以传递给 UI 框架组件！

15、插槽还可以呈现后备内容。当没有匹配的子元素传递到槽时， <slot /> 元素将呈现其自己的占位符子元素。

16、Astro 支持导入和使用 .html 文件作为组件，或将这些文件作为页面放置在 src/pages/ 子目录中。如果您要重用不使用框架构建的现有站点中的代码，或者想要确保组件没有动态功能，则可能需要使用 HTML 组件。

17、HTML 组件必须仅包含有效的 HTML，因此缺少关键的 Astro 组件功能。

18、HTML 组件内的 <slot /> 元素将像在 Astro 组件中一样工作。要改用 HTML Web 组件槽元素，请将 is:inline 添加到 <slot> 元素中。