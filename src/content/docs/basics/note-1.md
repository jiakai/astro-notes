---
title: Astro官方文档—Basics1学习笔记
description: Astro官方文档—Basics1学习笔记
---

1、astro的项目结构

![](https://cdn.jiakai.page/pictures/2024/05/09/202405091940346.webp)

2、src目录由astro构建处理

3、astro保留的唯一目录：`src/pages/`和`/src/content/`，其他任何目录都可以自由重命名和组织。

4、src/pages 是 Astro 项目中必需的子目录。没有它，您的网站将没有页面或路线！

5、可以将 CSS 和 JavaScript 放在 public/ 目录中，但请注意，这些文件不会在您的最终版本中捆绑或优化。

6、作为一般规则，您自己编写的任何 CSS 或 JavaScript 都应该位于您的 src/ 目录中。

7、项目树形结构

```
.
├── public(静态资源)
├── src(源代码所在位置)
│   ├── components(可重用代码单元，组件)
│   ├── layouts(布局组件)
│   ├── pages(用于创建新页面，可以是astro组件也可以是md文件)
│   │   └── posts
│   ├── styles(样式)
│   └── content(用于存储内容集合和可选的集合配置文件)
├── astro.config.mjs(Astro 项目的配置文件)
├── package.json(js包管理器用来管理依赖项的文件)
└── tsconfig.json(Astro 项目的 TypeScript 配置选项)
```