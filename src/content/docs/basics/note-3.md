---
title: Astro官方文档—Basics3学习笔记
description: Astro官方文档—Basics3学习笔记
---

1、pages的描述和支持的pages的页面类型

![](https://cdn.sa.net/2024/05/16/QrlMyL87NmspF9q.webp)

2、Astro 利用称为基于文件的路由的路由策略。 src/pages/ 目录中的每个文件根据其文件路径成为站点上的端点。

3、在 Astro 页面中编写标准 HTML <a> 元素以链接到网站上的其他页面。

4、Astro 页面使用 .astro 文件扩展名，并支持与 Astro 组件相同的功能。

5、页面必须生成完整的 HTML 文档。如果未明确包含，Astro 默认情况下会将必要的 <!DOCTYPE html> 声明和 <head> 内容添加到位于 src/pages/ 内的任何 .astro 组件。您可以通过将每个组件标记为部分页面来选择退出此行为。

6、为了避免在每个页面上重复相同的 HTML 元素，您可以将常见的 <head> 和 <body> 元素移动到您自己的布局组件中。您可以根据需要使用任意数量的布局组件。

7、src/content/ 中 Markdown 或 MDX 页面内容的集合可用于动态生成页面。

8、页面布局对于 Markdown 文件特别有用。 Markdown 文件可以使用特殊的 layout frontmatter 属性来指定布局组件，该组件将其 Markdown 内容包装在完整的 <html>...</html> 页面文档中。

9、具有 .html 文件扩展名的文件可以放置在 src/pages/ 目录中，并直接用作网站上的页面。请注意，HTML 组件不支持某些关键的 Astro 功能。

10、对于 404 错误页面，您可以在 /src/pages 中创建 404.astro 或 404.md 文件。

11、部分页面的详解见：https://docs.astro.build/en/basics/astro-pages/#page-partials
