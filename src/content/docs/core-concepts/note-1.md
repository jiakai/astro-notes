---
title: Astro官方文档—Core Concepts学习笔记
description: Astro官方文档—Core Concepts学习笔记
---

1、astro的features: [https://docs.astro.build/en/concepts/why-astro/#features](https://docs.astro.build/en/concepts/why-astro/#features)

2、astro框架的设计原则：

- 内容驱动

- 服务器优先

- 默认快速

- 易于使用

- 以开发人员为中心

3、.astro文件是html语言的超集。

4、Astro 开创并推广了一种名为 Islands 的前端架构。Islands 架构可帮助您避免单体 JavaScript 模式并自动从页面中剥离所有非必需的 JavaScript，从而提高前端性能。

5、astro islands的简要历史：[https://docs.astro.build/en/concepts/islands/#a-brief-history](https://docs.astro.build/en/concepts/islands/#a-brief-history)

6、什么是islands?

在astro中，island指的是页面上任何交互式UI组件。

7、虽然大多数开发者会坚持只使用一个 UI 框架，但 Astro 支持同一个项目中的多个框架。

8、默认情况下，Astro 会自动将每个 UI 组件渲染为 HTML 和 CSS，自动剥离所有客户端 JavaScript。

9、将任何静态 UI 组件转换为交互式孤island只需要一个 client:* 指令。然后，Astro 会自动构建并捆绑您的客户端 JavaScript，以优化性能。

```jsx
<MyReactComponent client:load />
```

10、用island时，客户端 JavaScript 仅针对使用 client:* 指令标记的显式交互组件加载。

11、由于交互是在组件级别配置的，因此您可以根据每个组件的使用情况处理不同的加载优先级。例如， `client:idle` 告诉组件在浏览器空闲时加载，`client:visible` 告诉组件仅在进入视口时加载。

12、使用island的好处

- 性能优化

- 并行加载

13、在 Astro 中，作为开发者的你，可以明确地告诉 Astro 页面上的哪些组件也需要在浏览器中运行。Astro 只会对页面上需要的内容进行水合，并将网站的其余部分保留为静态 HTML。