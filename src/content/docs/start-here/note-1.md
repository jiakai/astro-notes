---
title: Astro官方文档—Start Here学习笔记
description: Astro官方文档—Start Here学习笔记
---

1、用pnpm创建astro文档模版项目

```bash
// 使用 pnpm 创建一个基于 'starlight' 模板的 Astro 项目
pnpm create astro@latest -- --template starlight
```

-- --template如何理解：

TL;DR：前面的--表明后续参数不被pnpm处理，而是直接传递给create命令调用的具体脚本。

![](https://cdn.sa.net/2024/04/20/bWvPGUmNOnzeKut.webp)

2、astro官方入门模版地址：[https://astro.new/](https://astro.new/)

3、安装astro项目

```bash
// 使用 pnpm 创建一个基于最新版本 Astro 的项目，但没有指定模板
pnpm create astro@latest
```

注：安装向导会自动创建目录。

4、astro会监听src目录中的实时文件更改，因此在开发过程中进行更改时，无需重新启动服务器。

5、astro框架中的导入别名操作-[Import Aliases](https://docs.astro.build/en/guides/typescript/#import-aliases)

6、升级astro框架

```bash
# Upgrade Astro and official integrations together
pnpm dlx @astrojs/upgrade
```

7、语义版本控制系统

X.Y.Z:主版本、次版本、补丁版本

via: [Semantic versioning](https://docs.astro.build/en/upgrade-astro/#semantic-versioning)

