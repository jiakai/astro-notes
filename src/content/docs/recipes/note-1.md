---
title: Astro官方文档—Recipes学习笔记
description: Astro官方文档—Recipes学习笔记
---

1、Astro Studio Web 门户允许您通过 Web 界面或使用 CLI 命令连接到并管理远程托管的 Astro DB 数据库。

2、更多astro的使用技巧见官方文档：https://docs.astro.build/en/recipes/