---
title: Starlight官方文档—Start Here学习笔记
description: Starlight官方文档—Start Here学习笔记
---

1、在`src/content/docs`文件夹中创建markdown文件=将页面添加到网站。

2、更新starlight主题

```bash
# pnpm dlx 是用来直接从 npm 注册表执行一个包而不需要永久安装它。
# 这对于执行单次运行脚本或工具特别有用，比如在这个例子中升级 Astro 项目。
# 使用 pnpm 的 dlx 工具临时安装并运行 @astrojs/upgrade
# 这个命令用于升级 Astro 项目到最新版本
pnpm dlx @astrojs/upgrade
```

- [pnpm dlx](https://pnpm.io/cli/dlx)

3、starlight使用基于文件的路由。在`src/content/docs`文件夹中的markdown、mdx、markdoc文件都会变为网站上的一个页面。
frontmatter元数据会更改每个页面的显示方式。

:::note

什么是markdoc？

Markdoc 是一种基于 Markdown 的文档格式和内容发布框架。
Markdoc 通过标签和注释的自定义语法扩展了 Markdown，提供了一种为个人用户定制内容并引入交互式元素的方法。

via: [markdoc docs](https://markdoc.dev/)

:::