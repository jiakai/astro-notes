---
title: Starlight官方文档—Resources学习笔记
description: Starlight官方文档—Resources学习笔记
---

1、插件可以自定义 Starlight 配置、UI 和行为，同时也易于共享和重用。使用 Starlight 团队支持的官方插件和 Starlight 用户维护的社区插件扩展您的网站。

2、starlight的插件，可以与obsidian、ghost cms、notion集成，提供额外的丰富功能，如图片缩放插件。