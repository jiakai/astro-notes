---
title: Starlight官方文档—Guides学习笔记
description: Starlight官方文档—Guides学习笔记
---

1、markdown文件的frontmatter参考：[https://starlight.astro.build/reference/frontmatter/](https://starlight.astro.build/reference/frontmatter/)

2、自定义页面：[https://starlight.astro.build/guides/pages/#custom-pages](https://starlight.astro.build/guides/pages/#custom-pages)

3、旁白：[https://starlight.astro.build/guides/authoring-content/#asides](https://starlight.astro.build/guides/authoring-content/#asides)

4、更多的代码块功能：[https://starlight.astro.build/guides/authoring-content/#expressive-code-features](https://starlight.astro.build/guides/authoring-content/#expressive-code-features)

5、astro starlight主题的内置组件：[https://starlight.astro.build/guides/components/#built-in-components](https://starlight.astro.build/guides/components/#built-in-components)

6、starlight主题的项目结构：[https://starlight.astro.build/guides/project-structure/](https://starlight.astro.build/guides/project-structure/)