---
title: Astro官方文档—Build-ins2学习笔记
description: Astro官方文档—Build-ins2学习笔记
---

1、Astro 只需几行代码即可支持选择加入、每页、视图转换。视图转换无需浏览器正常的全页导航刷新即可更新页面内容，并在页面之间提供无缝动画。

2、Astro 提供了一个 <ViewTransitions /> 路由组件，可以将其添加到单个页面的 <head> 中，以在您导航到另一个页面时控制页面转换。它提供了一个轻量级的客户端路由器，可以拦截导航并允许您自定义页面之间的转换。

将此组件添加到可重用的 .astro 组件（例如通用头部或布局）中，以便在整个网站上实现动画页面转换（SPA 模式）。

3、astro的视图转换支持

![](https://cdn.sa.net/2024/05/16/LzQNKFR6ModjJIc.webp)

4、默认情况下，每个页面都将使用常规的全页浏览器导航。您必须选择查看过渡，并且可以在每个页面或整个站点范围内使用它们。

5、向页面添加视图转换

通过将 <ViewTransitions /> 路由组件导入并添加到每个所需页面上的 <head> ，选择在各个页面上使用视图转换。

6、完整站点视图转换（SPA 模式）

将 <ViewTransitions /> 组件导入并添加到常见的 <head> 或共享布局组件中。 Astro 将根据新旧页面之间的相似性创建默认页面动画，并且还将为不受支持的浏览器提供后备行为。

7、transition指令

Astro 会自动为旧页面和新页面中找到的相应元素分配一个共享的、唯一的 view-transition-name 。这对匹配元素是通过元素的类型及其在 DOM 中的位置来推断的。

astro组件使用可选的transition:*指令

![](https://cdn.sa.net/2024/05/16/VAg1xUiHY63uhte.webp)

8、命名一个transition

![](https://cdn.sa.net/2024/05/16/YyS9NrLwvEW6RAC.webp)

9、您可以使用 transition:persist 指令在页面导航中保留组件和 HTML 元素（而不是替换它们）。

10、您还可以将该指令放置在 Astro island（带有 client: 指令的 UI 框架组件）上。如果该组件存在于下一页上，则将继续显示旧页面中当前状态的岛，而不是用新页面中的岛替换它。

11、并非所有状态都可以通过这种方式保存。即使使用 transition:persist ，在视图转换期间也无法避免 CSS 动画的重新启动和 iframe 的重新加载。

12、作为一种方便的简写， transition:persist 也可以将transition名称作为值。

13、内置的animation指令

![](https://cdn.sa.net/2024/05/16/yjVE7qoACedhgK8.webp)

14、路由控制

![](https://cdn.sa.net/2024/05/16/EnGOlM8zvq41FyI.webp)

15、防止客户端的导航

![](https://cdn.sa.net/2024/05/16/V5qgHrEuAWSP3tX.webp)

16、其余内容见：https://docs.astro.build/en/guides/view-transitions/