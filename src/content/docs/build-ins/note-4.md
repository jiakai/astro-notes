---
title: Astro官方文档—Build-ins4学习笔记
description: Astro官方文档—Build-ins4学习笔记
---

1、有关astro开发者工具栏的介绍

![](https://cdn.sa.net/2024/05/18/5krqlQspBRON8nH.webp)

2、禁用开发者工具栏：要为项目中的每个人禁用开发工具栏，请在 Astro 配置文件中设置 devToolbar: false 。