---
title: Astro官方文档—Build-ins1学习笔记
description: Astro官方文档—Build-ins1学习笔记
---

1、内容集合是在任何 Astro 项目中管理和创作内容的最佳方式。集合有助于组织文档、验证 frontmatter 并为所有内容提供自动 TypeScript 类型安全。

2、内容集合是保留的 src/content 项目目录内的任何顶级目录，例如 src/content/newsletter 和 src/content/authors 。 src/content 目录中只允许内容集合。该目录不能用于其他任何用途。

3、集合条目

![](https://cdn.sa.net/2024/05/16/xpDPGlCi5SQIYuL.webp)

4、.astro目录

![](https://cdn.sa.net/2024/05/16/9eFtdAHSG5ncP1R.webp)

5、组织多个集合

![](https://cdn.sa.net/2024/05/16/GE78PYbtApd5fCO.webp)

6、使用子目录进行组织

![](https://cdn.sa.net/2024/05/16/d6LouMDfwTOsvzF.webp)

7、定义集合

![](https://cdn.sa.net/2024/05/16/auG2gSkxrZcPy31.webp)

8、如果您尚未在 tsconfig.json 文件中扩展 Astro 的 strict 或 strictest 推荐 TypeScript 设置，则可能需要将 tsconfig.json 更新为启用 strictNullChecks 。

9、tsconfig.json中配置允许js。

![](https://cdn.sa.net/2024/05/16/4NO6PLVbFMiRXfI.webp)

10、定义集合模式

模式强制集合中一致的前题或条目数据。当您需要引用或查询该数据时，架构可保证该数据以可预测的形式存在。如果任何文件违反其集合架构，Astro 将提供有用的错误来通知您。

模式还为 Astro 的内容自动 TypeScript 类型提供支持。当您为集合定义架构时，Astro 将自动生成 TypeScript 接口并将其应用到其中。当您查询集合时，结果是完整的 TypeScript 支持，包括属性自动完成和类型检查。

![](https://cdn.sa.net/2024/05/16/CANm7rMowB8svFg.webp)

11、定义多个集合

![](https://cdn.sa.net/2024/05/16/rYIdb5USWmL3xep.webp)

![](https://cdn.sa.net/2024/05/16/kUqP7u5nyAK6fiM.webp)

12、使用第三方collection模式

您可以从任何地方导入集合架构，包括外部 npm 包。当使用提供自己的集合架构供您使用的主题和库时，这非常有用。

13、使用 Zod 定义数据类型

Astro 使用 Zod 为其内容模式提供支持。借助 Zod，Astro 能够验证集合中每个文件的 frontmatter，并在您从项目内部查询内容时提供自动 TypeScript 类型。

14、定义集合引用

集合条目还可以“引用”其他相关条目。

使用 Collections API 中的 reference() 函数，您可以将集合架构中的属性定义为另一个集合中的条目。例如，您可以要求每个 space-shuttle 条目都包含一个 pilot 属性，该属性使用 pilot 集合自己的架构进行类型检查、自动完成和验证。

15、定义自定义的slug

使用 type: 'content' 时，每个内容条目都会从其文件 id 生成 URL 友好的 slug 属性。 slug 用于直接从您的集合中查询条目。当从您的内容创建新页面和 URL 时，它也很有用。

您可以通过将自己的 slug 属性添加到文件 frontmatter 来覆盖条目生成的 slug。这类似于其他 Web 框架的“永久链接”功能。 "slug" 是一个特殊的保留属性名称，在您的自定义集合 schema 中不允许使用，并且不会出现在您条目的 data 属性中。

16、查询集合

Astro 提供了两个函数来查询集合并返回一个（或多个）内容条目： getCollection() 和 getEntry() 。

这两个函数都返回由 CollectionEntry 类型定义的内容条目。

17、访问参考的数据

首次查询集合条目后，必须单独查询架构中定义的任何引用。您可以再次使用 getEntry() 函数或 getEntries() 来从返回的 data 对象中检索引用的条目。

18、过滤集合查询

getCollection() 采用可选的“过滤器”回调，允许您根据条目的 id 或 data （frontmatter）属性过滤查询。对于 type: 'content' 的集合，您还可以根据 slug 进行过滤。

slug 属性特定于内容集合，在过滤 JSON 或 YAML 集合时不可用。

您可以使用它来按您喜欢的任何内容标准进行过滤。例如，您可以按 draft 等属性进行过滤，以防止任何草稿博客文章发布到您的博客。

19、使用astro模版中的内容

查询集合条目后，您可以直接在 Astro 组件模板内访问每个条目。这使您可以呈现 HTML 内容，例如指向内容的链接（使用内容 slug ）或有关内容的信息（使用 data 属性）。

20、将内容作为props传递

组件还可以将整个内容条目作为 prop 传递。

21、查询后，您可以使用条目 render() 函数属性将 Markdown 和 MDX 条目呈现为 HTML。调用此函数使您可以访问呈现的内容和元数据，包括 <Content /> 组件和所有呈现的标题的列表。

22、内容集合存储在 src/pages/ 目录之外。这意味着默认情况下不会为您的集合项生成任何路由。您将需要手动创建新的动态路由以从集合条目生成 HTML 页面。您的动态路由将映射传入的请求参数（例如： src/pages/blog/[...slug].astro 中的 Astro.params.slug ）以获取集合中的正确条目。

生成路由的确切方法取决于您的构建 output 模式：“static”（默认）或“server”（对于 SSR）

23、构建静态输出（默认）

如果您正在构建静态网站（Astro 的默认行为），则可以在构建过程中使用 getStaticPaths() 函数从单个 src/pages/ 组件创建多个页面。

调用 getStaticPaths() 内部的 getCollection() 来查询您的内容或数据集合。然后，使用每个内容条目的 slug 属性（内容集合）或 id 属性（数据集合）创建新的 URL 路径。

如果您的自定义 slug 包含 / 字符来生成具有多个路径段的 URL，则必须在 .astro 文件名中为此动态使用剩余参数 ( [...slug] )路由页面。

24、构建服务器端输出（SSR）

如果您正在构建动态网站（使用 Astro 的 SSR 支持），则不需要在构建过程中提前生成任何路径。相反，您的页面应该检查请求（使用 Astro.request 或 Astro.params ）以按需查找 slug ，然后使用 getEntry()

25、从基于文件的路由迁移

如果您有一个现有的 Astro 项目（例如博客），在 src/pages/ 内的子文件夹中使用 Markdown 或 MDX 文件，请考虑将相关内容或数据文件迁移到内容集合。

26、启用构建缓存

如果您正在处理大型集合，您可能希望使用 experimental.contentCollectionCache 标志启用缓存构建。这一实验性功能优化了 Astro 的构建过程，使未更改的集合能够在构建之间存储和重用。

27、使用remark修改frontmatter

不建议。 Remark 和 rehype 插件访问原始 Markdown 或 MDX 文档 frontmatter。这意味着 remarkPluginFrontmatter frontmatter 与类型安全的 schema 分开处理，并且不会反映通过 Astro 应用的任何更改或默认值。使用风险自负！

remark 和 rehype 管道仅在呈现内容时运行，这解释了为什么 remarkPluginFrontmatter 仅在您对内容条目调用 render() 后才可用。相反， getCollection() 和 getEntry() 无法直接返回这些值，因为它们不呈现您的内容。

28、在frontmatter中处理日期

内容集合中可以使用多种日期格式，但集合的架构必须与 Markdown 或 MDX YAML frontmatter 中使用的格式匹配。

YAML 使用 ISO-8601 标准来表达日期。使用格式 yyyy-mm-dd （例如 2021-07-28 ）以及架构类型 z.date()

如果未提供时区，则日期格式将以 UTC 格式指定。如果需要指定时区，可以使用 ISO 8601 格式。

要仅渲染完整 UTC 时间戳中的 YYYY-MM-DD ，请使用 JavaScript slice 方法删除时间戳。