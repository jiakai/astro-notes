---
title: Astro官方文档—Build-ins2学习笔记
description: Astro官方文档—Build-ins2学习笔记
---

1、页面加载时间对于网站的可用性和整体享受起着重要作用。当访问者与网站交互时，Astro 的选择加入预取功能可为您的多页面应用程序 (MPA) 带来近乎即时的页面导航优势。

2、启用prefetch

![](https://cdn.sa.net/2024/05/18/CgmrYvhH3VPQGMo.webp)

3、astro支持的prefetch策略

![](https://cdn.sa.net/2024/05/18/SDAXMVygTHoKROq.webp)

4、默认的prefetch策略

![](https://cdn.sa.net/2024/05/18/9rLJUhmGyadij1C.webp)

5、默认prefetch所有链接

![](https://cdn.sa.net/2024/05/18/VAXOqorPiNj9sYe.webp)

6、当您在页面上使用视图转换时，默认情况下也会启用预取。它设置了默认配置 { prefetchAll: true } ，可以预取页面上的所有链接。