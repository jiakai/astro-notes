import { defineConfig } from 'astro/config';
import starlight from '@astrojs/starlight';

// https://astro.build/config
export default defineConfig({
	site: "https://astro.gujiakai.top",
	outDir: 'public',
	publicDir: 'static',
	integrations: [
		starlight({
			title: 'Astro学习笔记',
			editLink: {
				baseUrl: 'https://gitlab.com/jiakai/astro-notes/-/edit/main/',
			  },
			social: {
				github: 'https://gitlab.com/jiakai/astro-notes',
			},
			sidebar: [
				{ label: 'Start Here', link: '/start-here/note-1/' },
				{ label: 'Core Concepts', link: '/core-concepts/note-1/' },
				{
					label: 'Basics', 
					items: [
						{ label: 'Note 1', link: '/basics/note-1/' },
						{ label: 'Note 2', link: '/basics/note-2/' },
						{ label: 'Note 3', link: '/basics/note-3/' },
						{ label: 'Note 4', link: '/basics/note-4/' },
						{ label: 'Note 5', link: '/basics/note-5/' },
						{ label: 'Note 6', link: '/basics/note-6/' }
					]
				},
				{
					label: 'Build-ins',
					items: [
						{ label: 'Note 1', link: '/build-ins/note-1/' },
						{ label: 'Note 2', link: '/build-ins/note-2/' },
						{ label: 'Note 3', link: '/build-ins/note-3/' },
						{ label: 'Note 4', link: '/build-ins/note-4/' }
					]
				},
				{ label: 'Recipes', link: '/recipes/note-1/' },
				{
					label: 'Guides',
					items: [
						{ label: 'Note 1', link: '/guides/note-1/' },
						{ label: 'Note 2', link: '/guides/note-2/' },
						{ label: 'Note 3', link: '/guides/note-3/' },
						{ label: 'Note 4', link: '/guides/note-4/' }
					]
				},
				{
					label: 'Starlight',
					items: [
						{ label: 'Start Here', link: '/starlight/note-1/' },
						{ label: 'Guides', link: '/starlight/note-2/' },
						{ label: 'Reference', link: '/starlight/note-3/' },
						{ label: 'Resources', link: '/starlight/note-4/' }
					]
				}
			],
			lastUpdated: true
		}),
	],
});
